<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>
<xsl:include href="intern:bswsp:~param/libMoodle/img.xsl" />
<xsl:include href="intern:bswsp:~param/libMoodle/mathtex.xsl" />

<xsl:template match="sc:para">
  <p><xsl:apply-templates select="sc:*|text()"/></p>
</xsl:template>

<xsl:template match="text()">
  <xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="sc:itemizedList">
  <ul><xsl:apply-templates select="sc:*|text()"/></ul>
</xsl:template>

<xsl:template match="sc:orderedList">
  <ol><xsl:apply-templates select="sc:*|text()"/></ol>
</xsl:template>

<xsl:template match="sc:listItem">
  <li><xsl:apply-templates select="sc:*|text()"/></li>
</xsl:template>

<xsl:template match="sc:table">
  <table style="border-collapse: collapse; margin: auto; "><tbody><xsl:apply-templates select="sc:row"/></tbody></table>
  <xsl:apply-templates select="sc:caption" />
</xsl:template>

<xsl:template match="sc:row">
  <tr><xsl:apply-templates select="sc:cell"/></tr>
</xsl:template>

<xsl:template match="sc:cell">
  <xsl:choose>
    <xsl:when test="./@role = 'word'">
      <td style="border: solid 1px black; text-align: center;"><xsl:apply-templates select="sc:*|text()"/></td>
    </xsl:when>
    <xsl:when test="./@role = 'num'">
      <td style="border: solid 1px black; text-align: right;"><xsl:apply-templates select="sc:*|text()"/></td>
    </xsl:when>
    <xsl:otherwise>
      <td style="border: solid 1px black; text-align: left;"><xsl:apply-templates select="sc:*|text()"/></td>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="sc:row[@role='head']/sc:cell">
  <xsl:choose>
    <xsl:when test="./@role = 'word'">
      <th style="border: solid 1px black; text-align: center;"><xsl:apply-templates select="sc:*|text()"/></th>
    </xsl:when>
    <xsl:when test="./@role = 'num'">
      <th style="border: solid 1px black; text-align: right;"><xsl:apply-templates select="sc:*|text()"/></th>
    </xsl:when>
    <xsl:otherwise>
      <th style="border: solid 1px black; text-align: left;"><xsl:apply-templates select="sc:*|text()"/></th>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="sc:caption">
  <p style="text-align: center; font-style: italic; color: gray;">
    <xsl:value-of select="." disable-output-escaping="yes" />
  </p>
</xsl:template>

<!-- gestion des liens -->
<xsl:template match="sc:phrase[@role='url']">
  <xsl:choose>
    <xsl:when test="op:urlM/sp:title"><a href="{op:urlM/sp:url}" title="{op:urlM/sp:title}" target="_blank"><xsl:value-of select="./child::text()" /></a></xsl:when>
    <xsl:otherwise><a href="{op:urlM/sp:url}" target="_blank"><xsl:value-of select="./child::text()" /></a></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- gestion des glossaires -->
<xsl:template match="sc:uLink[@role='glos']">
	<xsl:value-of select="." disable-output-escaping="yes"/>
</xsl:template>

<!-- gestion des inlineStyle -->
<xsl:template match="sc:inlineStyle[@role='quote']">
  « <xsl:apply-templates select="sc:*|text()"/> »
</xsl:template>

<xsl:template match="sc:inlineStyle[@role='emp']">
  <strong><xsl:apply-templates select="sc:*|text()"/></strong>
</xsl:template>

<xsl:template match="sc:inlineStyle[@role='spec']">
  <em><xsl:apply-templates select="sc:*|text()"/></em>
</xsl:template>

<xsl:template match="sc:inlineStyle[@role='code']">
  <code><xsl:apply-templates select="sc:*|text()"/></code>
</xsl:template>


<!-- gestion des textLeaf -->
<xsl:template match="sc:textLeaf[@role='ind']">
  <sub><xsl:apply-templates select="sc:*|text()"/></sub>
</xsl:template>

<xsl:template match="sc:textLeaf[@role='exp']">
  <sup><xsl:apply-templates select="sc:*|text()"/></sup>
</xsl:template>

<xsl:template match="sc:textLeaf[@role='mathtex']">
  <xsl:call-template name="mathtex">
    <xsl:with-param name="equation" select="." />
    <xsl:with-param name="display" select="'inline'" />
  </xsl:call-template>
</xsl:template>


<!-- gestion des inlineImg -->
<xsl:template match="sc:inlineImg[@role='form']">
  <xsl:if test="contains(@sc:refUri,'.mtex')">
    <xsl:call-template name="mathtex">
      <xsl:with-param name="equation" select="getContent(gotoSubModel(),'')" />
      <xsl:with-param name="display" select="'inline'" />
    </xsl:call-template>
  </xsl:if>
  <xsl:if test="contains(@sc:refUri,'.odf')">
    <xsl:call-template name="image">
      <xsl:with-param name="axis" select="'ico'" />
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<xsl:template match="sc:inlineImg[@role='ico']">
  <xsl:call-template name="image">
    <xsl:with-param name="axis" select="'ico'" />
  </xsl:call-template>
</xsl:template>

</xsl:stylesheet>