<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">

<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>

<xsl:variable name="maxlength" select="100" />

<xsl:template match="sc:question">
  <xsl:call-template name="cdata">
    <xsl:with-param name="elt" select="'questiontext'" />
  </xsl:call-template>
</xsl:template>

<xsl:template match="sc:question" mode="title">
  <xsl:variable name="title" select="getContent(gotoSubModel(),'title')"/>
  <xsl:choose>
    <xsl:when test="string-length($title) > $maxlength"><xsl:value-of select="concat(substring($title,1,$maxlength),'…')" /></xsl:when>
    <xsl:otherwise><xsl:value-of select="$title" /></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="sc:globalExplanation">
  <xsl:call-template name="cdata">
    <xsl:with-param name="elt" select="'generalfeedback'" />
  </xsl:call-template>
</xsl:template>

<xsl:template name="cdata">
  <xsl:param name="elt"/>
  <xsl:variable name="init_files" select="setDialogVar('files','')"/>
  <xsl:element name="{$elt}">
    <xsl:attribute name="format">html</xsl:attribute>
    <text><xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
		<xsl:if test="$exportExpGlob='yes' and $elt='feedback'">
			<xsl:value-of select="getContent(gotoSubModel(//sc:globalExplanation),'')" disable-output-escaping="yes"/>
		</xsl:if></text>
    <xsl:value-of select="getDialogVar('files')" disable-output-escaping="yes"/>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
