<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" exclude-result-prefixes="sc sp op">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:variable name="exportMeta" select="getDialogVar('exportMeta')"/>
	<xsl:template match="op:exeM">
		<tags>
			<xsl:apply-templates select="sp:themeLicence"/>
			<xsl:apply-templates select="sp:level"/>
			<xsl:apply-templates select="sp:educationLevel"/>
			<xsl:apply-templates select="sp:info"/>
		</tags>
	</xsl:template>
	<xsl:template match="sp:info">
		<xsl:value-of select="getContent(gotoMeta(),'tags')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="sp:level">
		<xsl:choose>
			<xsl:when test="$exportMeta = 'yes'">
				<tag>
					<text>complexité:<xsl:value-of select="."/>
					</text>
				</tag>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="sp:educationLevel">
		<xsl:choose>
			<xsl:when test="$exportMeta = 'yes'">
				<tag>
					<text>niveau:<xsl:value-of select="."/>
					</text>
				</tag>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="sp:themeLicence">
		<xsl:choose>
			<xsl:when test="$exportMeta = 'yes'">
				<xsl:choose>
					<!-- POUR GENERER CETTE LISTE, automatiquement à partir des fichiers sources de topoze -->
					<!-- dans le répertoire récupéré de SVN de topoze, depuis un terminal sous linux : -->
					<!-- cat "model/sources/topoze/auth/search/critrion.doss/themeLicence.xml" | grep 'sm:option' | perl -pe 's/(.*key=")(.*)(" name=".*;[^\w]*)(.*?)([^\w]*")(.*)/<xsl:when test=".='\''$2'\''"><tag><text>thème:$4<\/text><\/tag><\/xsl:when>/g' -->
					<!-- réparer le xml cassé (retapper la ligne numérique), remplacer "," par ";" pas supporté dans les tags moodle... -->
					<!-- rajouter un carractère mangé lorsque le thème se fini par quelque chose de pas \w : é (relativité) ou par une ) fermante, programmation C++ par ex... -->
					<xsl:when test=".='#chim-'">
						<tag>
							<text>thème:Chimie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-strucmat-'">
						<tag>
							<text>thème:Structure de la matière</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-chimorga-'">
						<tag>
							<text>thème:Chimie organique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-chimsolu-'">
						<tag>
							<text>thème:Chimie des solutions</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-chimexpetech-'">
						<tag>
							<text>thème:Chimie expérimentale et techniques de laboratoire</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-thermochim-'">
						<tag>
							<text>thème:Thermochimie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-stereochim-'">
						<tag>
							<text>thème:Stéréochimie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-cinechim-'">
						<tag>
							<text>thème:Cinétique chimique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-spectrosco-'">
						<tag>
							<text>thème:Spectroscopies</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-chimquant-'">
						<tag>
							<text>thème:Chimie quantique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-chimsoli-'">
						<tag>
							<text>thème:Chimie du solide</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-chiminorg-'">
						<tag>
							<text>thème:Chimie inorganique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-electrochim-'">
						<tag>
							<text>thème:Electrochimie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-plymmat-'">
						<tag>
							<text>thème:Polymères et matériaux</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#chim-chimindus-'">
						<tag>
							<text>thème:Chimie industrielle</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-'">
						<tag>
							<text>thème:Mathématiques</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-algo-'">
						<tag>
							<text>thème:Algorithmique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-lograis-'">
						<tag>
							<text>thème:Logique et raisonnements</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-arithm-'">
						<tag>
							<text>thème:Arithmétique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-suites-'">
						<tag>
							<text>thème:Suites</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-series-'">
						<tag>
							<text>thème:Séries</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-fonctions-'">
						<tag>
							<text>thème:Fonctions</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-derivation-'">
						<tag>
							<text>thème:Dérivation</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-equa-'">
						<tag>
							<text>thème:Equations/Inéquations</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-nbreelscompl-'">
						<tag>
							<text>thème:Nombres réels et complexes</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-integration-'">
						<tag>
							<text>thème:Intégration</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-polyno-'">
						<tag>
							<text>thème:Polynomes</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-alglin-'">
						<tag>
							<text>thème:Algèbre linéaire</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-geo-'">
						<tag>
							<text>thème:Géométrie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-trigonometrie-'">
						<tag>
							<text>thème:Trigonométrie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-stat-'">
						<tag>
							<text>thème:Statistique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#math-proba-'">
						<tag>
							<text>thème:Probabilités</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-'">
						<tag>
							<text>thème:Physique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-forcemvt-'">
						<tag>
							<text>thème:Force et mouvement</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-loiscons-'">
						<tag>
							<text>thème:Lois de conservation</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-loisnewton-'">
						<tag>
							<text>thème:Lois de Newton et Kepler et leurs applications</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-mecasol-'">
						<tag>
							<text>thème:Mécanique du solide</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-mecafluides-'">
						<tag>
							<text>thème:Mécanique des fluides</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-oscilreson-'">
						<tag>
							<text>thème:Oscillateurs; résonance</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-acous-'">
						<tag>
							<text>thème:Acoustique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-optigeom-'">
						<tag>
							<text>thème:Optique géométrique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-thermodyn-'">
						<tag>
							<text>thème:Thermodynamique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-ondes-'">
						<tag>
							<text>thème:Physique des ondes</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-electrostat-'">
						<tag>
							<text>thème:Electrostatique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-electrocin-'">
						<tag>
							<text>thème:Electrocinétique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-traitsign-'">
						<tag>
							<text>thème:Traitement du signal</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-magnetism-'">
						<tag>
							<text>thème:Magnétisme</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-electromagn-'">
						<tag>
							<text>thème:Electromagnétisme</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-elecana-'">
						<tag>
							<text>thème:Electronique analogique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-electron-'">
						<tag>
							<text>thème:Electronique numérique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-courelec-'">
						<tag>
							<text>thème:Courant électrique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-condohm-'">
						<tag>
							<text>thème:Conducteurs ohmiques</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-reslin-'">
						<tag>
							<text>thème:Réseaux linéaires</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-physnucl-'">
						<tag>
							<text>thème:Physique nucléaire</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-cristall-'">
						<tag>
							<text>thème:Cristallographie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-physquant-'">
						<tag>
							<text>thème:Physique quantique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-relativite-'">
						<tag>
							<text>thème:Relativité</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-mesinstrinc-'">
						<tag>
							<text>thème:Mesure; instrumentation et incertitudes</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#phys-mathphys-'">
						<tag>
							<text>thème:Mathématiques pour la physique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-'">
						<tag>
							<text>thème:Biologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-bioanim-'">
						<tag>
							<text>thème:Biologie animale</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-bioveget-'">
						<tag>
							<text>thème:Biologie végétale</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-cellule-'">
						<tag>
							<text>thème:Cellule structure et fonctions</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-angenome-'">
						<tag>
							<text>thème:Acides nucléiques; génomes et divisions cellulaires (mitose; méiose)</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-evoclasbiod-'">
						<tag>
							<text>thème:Evolution – classification – biodiversit</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-biomol-'">
						<tag>
							<text>thème:Biochimie et biologie moléculaire</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-microbio-'">
						<tag>
							<text>thème:Microbiologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-immuno-'">
						<tag>
							<text>thème:Immunologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-ecolenvir-'">
						<tag>
							<text>thème:Ecologie – Environnement</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-techbiotech-'">
						<tag>
							<text>thème:Techniques d'études et biotechnologies</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-phyanim-'">
						<tag>
							<text>thème:Physiologie animale</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#bio-phyvege-'">
						<tag>
							<text>thème:Physiologie végétale</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-'">
						<tag>
							<text>thème:Géosciences</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-tecto-'">
						<tag>
							<text>thème:Tectonique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-geody-'">
						<tag>
							<text>thème:Géodynamique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-miner-'">
						<tag>
							<text>thème:Minéralogie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-hydro-'">
						<tag>
							<text>thème:Hydrologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-clima-'">
						<tag>
							<text>thème:Climatologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-petrol-'">
						<tag>
							<text>thème:Pétrologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-geoph-'">
						<tag>
							<text>thème:Géophysique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-geoch-'">
						<tag>
							<text>thème:Géochimie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-planet-'">
						<tag>
							<text>thème:Planétologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-cosmo-'">
						<tag>
							<text>thème:Cosmologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-geote-'">
						<tag>
							<text>thème:Géologie de terrain</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-paleonto-'">
						<tag>
							<text>thème:Paléontologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-sedimento-'">
						<tag>
							<text>thème:Sédimentologie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#geo-stratigra-'">
						<tag>
							<text>thème:Stratigraphie</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-'">
						<tag>
							<text>thème:Informatique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-logiq-'">
						<tag>
							<text>thème:Logique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-algor-'">
						<tag>
							<text>thème:Algorithmique</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-c-'">
						<tag>
							<text>thème:Programmation C</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-cpp-'">
						<tag>
							<text>thème:Programmation C++</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-python-'">
						<tag>
							<text>thème:Programmation Python</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-java-'">
						<tag>
							<text>thème:Programmation Java</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-python-'">
						<tag>
							<text>thème:Programmation Python</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-jsc-'">
						<tag>
							<text>thème:Programmation JavaScript</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-html-'">
						<tag>
							<text>thème:Langage HTML</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-css-'">
						<tag>
							<text>thème:Langage CSS</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-unixlike-'">
						<tag>
							<text>thème:Systèmes de type UNIX</text>
						</tag>
					</xsl:when>
					<xsl:when test=".='#info-bdd-'">
						<tag>
							<text>thème:Bases de données</text>
						</tag>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>
