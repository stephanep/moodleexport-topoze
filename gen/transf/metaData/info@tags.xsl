<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" exclude-result-prefixes="sc sp op">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:variable name="exportMeta" select="getDialogVar('exportMeta')"/>
	<xsl:template match="op:info">
		<xsl:apply-templates select="sp:cpyrgt"/>
		<xsl:apply-templates select="sp:keywds"/>
	</xsl:template>
	<xsl:template match="sp:cpyrgt">
<xsl:choose>
<xsl:when test="$exportMeta = 'true'">
		<tag>
			<text>origine:<xsl:value-of select="translate(., ',',';')"/>
			</text>
		</tag>
</xsl:when>
<xsl:otherwise>
</xsl:otherwise>
</xsl:choose>
	</xsl:template>
	<xsl:template match="sp:keywds">
		<xsl:value-of select="getContent(gotoMeta(),'tags')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>