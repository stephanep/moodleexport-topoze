<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" exclude-result-prefixes="sc sp op">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:template match="op:keywds">
		<xsl:if test="sp:keywd">
			<xsl:apply-templates select="sp:keywd"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="sp:keywd">
		<tag>
			<text>
				<xsl:value-of select="translate(., ',',';')"/>
			</text>
		</tag>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>
