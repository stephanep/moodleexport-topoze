<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />
  <xsl:param name="vDialog" />
  <xsl:param name="vAgent" />
  <xsl:include href="intern:bswsp:~param/libMoodle/img.xsl" />

  <xsl:template match="op:res">
    <xsl:apply-templates select="sp:txt"/>
  </xsl:template>

  <xsl:template match="sp:txt">
    <xsl:value-of select="getContent(gotoSubModel(),'text')" disable-output-escaping="yes" />
  </xsl:template>

  <xsl:template match="sp:filtered">
    <xsl:value-of select="getContent(gotoSubModel(),'title')" disable-output-escaping="yes" />
  </xsl:template>

</xsl:stylesheet>

