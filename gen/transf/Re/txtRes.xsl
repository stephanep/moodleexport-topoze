<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
  <xsl:param name="vDialog"/>
  <xsl:param name="vAgent"/>
  <xsl:include href="intern:bswsp:~param/libMoodle/img.xsl" />

  <xsl:variable name="pos" select="getContent(gotoMeta(),'')"/>

  <xsl:template match="op:txtRes">
    <div>
      <xsl:apply-templates select="sp:img"/>
      <xsl:apply-templates select="sp:txt"/>
      <div style="clear:both;"/>
    </div>
  </xsl:template>

  <xsl:template match="sp:img">

    <xsl:call-template name="image">
      <xsl:with-param name="axis" select="'txtRes'" />
      <xsl:with-param name="pos" select="$pos" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="sp:txt">
    <xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes" />
  </xsl:template>

  <xsl:template match="*"/>

</xsl:stylesheet>
