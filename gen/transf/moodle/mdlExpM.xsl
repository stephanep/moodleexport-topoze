<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" xmlns:opa="kelis.fr:opa" exclude-result-prefixes="sc sp op opa">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:template match="opa:mdlExpM">
		<!-- Forcer l'ordre des questions dans l'import moodle -->
		<xsl:choose>
			<xsl:when test="sp:forceQuizOrder">
				<xsl:variable name="forceQuizOrder" select="setDialogVar('forceQuizOrder',sp:forceQuizOrder)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="forceQuizOrderDefault" select="setDialogVar('forceQuizOrder','no')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- Reprendre le nom d'item Scenari dans le titre de l'exercice moodle -->
		<xsl:choose>
			<xsl:when test="sp:QuizNameInTitle">
				<xsl:variable name="QuizNameInTitle" select="setDialogVar('QuizNameInTitle',sp:QuizNameInTitle)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="QuizNameInTitleDefault" select="setDialogVar('QuizNameInTitle','no')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- mélange des questions -->
		<xsl:choose>
			<xsl:when test="sp:shuffle">
				<xsl:variable name="shuffle" select="setDialogVar('shuffle',sp:shuffle)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="shuffleDefault" select="setDialogVar('shuffle','no')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- numérotation des réponses -->
		<xsl:choose>
			<xsl:when test="sp:numbering">
				<xsl:variable name="numbering" select="setDialogVar('numbering',sp:numbering)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="numberingDefault" select="setDialogVar('numbering','none')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- texte riche dans les réponses -->
		<xsl:choose>
			<xsl:when test="sp:richtext">
				<xsl:variable name="richtext" select="setDialogVar('richtext',sp:richtext)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="richtextDefault" select="setDialogVar('richtext','true')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- format d'export des questions d'ordonnancement -->
		<xsl:choose>
			<xsl:when test="sp:ordWordType">
				<xsl:variable name="ordWordType" select="setDialogVar('ordWordTypeGlobal',sp:ordWordType)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="ordWordTypeDefault" select="setDialogVar('ordWordTypeGlobal','clozeP')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- organisation automatique en catégories -->
		<xsl:choose>
			<xsl:when test="sp:organizing">
				<xsl:variable name="organizing" select="setDialogVar('organizing',sp:organizing)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="organizingDefault" select="setDialogVar('organizing','false')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- choix du moteur LaTeX -->
		<xsl:choose>
			<xsl:when test="sp:latex">
				<xsl:variable name="latex" select="setDialogVar('latex',sp:latex)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="latexDefault" select="setDialogVar('latex','mimetex')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- choix d'exporter les MD en mots clés -->
		<xsl:choose>
			<xsl:when test="sp:exportMeta">
				<xsl:variable name="exportMeta" select="setDialogVar('exportMeta',sp:exportMeta)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="exportMetaDefault" select="setDialogVar('exportMeta','yes')"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- choix d'exporter les explications globales -->
		<xsl:choose>
			<xsl:when test="sp:exportExpGlob">
				<xsl:variable name="exportExpGlob" select="setDialogVar('exportExpGlob',sp:exportExpGlob)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="exportExpGlobDefault" select="setDialogVar('exportExpGlob','true')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>
