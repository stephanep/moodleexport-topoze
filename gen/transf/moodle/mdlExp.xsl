<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" xmlns:opa="kelis.fr:opa" exclude-result-prefixes="sc sp op opa">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:template match="opa:mdlExp">
		<!-- si les paramètres ne sont pas définis, on crée les valeurs par défaut -->
		<xsl:choose>
			<xsl:when test="opa:mdlExpM">
				<xsl:variable name="meta" select="getContent(gotoMeta(),'')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="forceQuizOrderDefault" select="setDialogVar('forceQuizOrder','false')"/>
				<xsl:variable name="QuizNameInTitleDefault" select="setDialogVar('QuizNameInTitle','false')"/>
				<xsl:variable name="shuffleDefault" select="setDialogVar('shuffle','false')"/>
				<xsl:variable name="numberingDefault" select="setDialogVar('numbering','none')"/>
				<xsl:variable name="richtextDefault" select="setDialogVar('richtext','true')"/>
				<xsl:variable name="organizingDefault" select="setDialogVar('organizing','false')"/>
				<xsl:variable name="exportMetaDefault" select="setDialogVar('exportMeta','false')"/>
				<xsl:variable name="exportExpGlobDefault" select="setDialogVar('exportExpGlob','no')"/>
				<xsl:variable name="latexDefault" select="setDialogVar('latex','mimetex')"/>
				<xsl:variable name="ordWordTypeDefault" select="setDialogVar('ordWordTypeGlobal','clozeP')"/>
			</xsl:otherwise>
		</xsl:choose>
		<quiz>
			<xsl:choose>
				<xsl:when test="getDialogVar('organizing') = 'true'">
					<xsl:apply-templates select="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcMatch | sp:trainUcOrdWord | sp:trainUcCloze | sp:trainUcField | sp:trainUcNumerical | sp:group"/>
					<xsl:apply-templates select="sp:group | sp:cat" mode="category"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcMatch | sp:trainUcOrdWord | sp:trainUcCloze | sp:trainUcField | sp:trainUcNumerical | sp:group"/>
					<xsl:apply-templates select="sp:cat" mode="category"/>
				</xsl:otherwise>
			</xsl:choose>
		</quiz>
	</xsl:template>
	<xsl:template match="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcMatch | sp:trainUcCloze | sp:trainUcField | sp:trainUcNumerical | sp:group">
		<xsl:variable name="vPos" select="setDialogVar('vPos', position())"/>
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="sp:trainUcOrdWord">
		<xsl:variable name="vPos" select="setDialogVar('vPos', position())"/>
		<xsl:variable name="globalType" select="getDialogVar('ordWordTypeGlobal')"/>
		<xsl:variable name="localType" select="normalize-space(getContent(gotoMeta(),''))"/>
		<xsl:if test="string-length($localType) = 0">
			<xsl:variable name="type" select="setDialogVar('ordWordType',$globalType)"/>
		</xsl:if>
		<xsl:if test="string-length($localType) &gt; 0">
			<xsl:variable name="type" select="setDialogVar('ordWordType',$localType)"/>
		</xsl:if>
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="sp:group" mode="category">
		<xsl:variable name="fullName" select="setDialogVar('fullName','')"/>
		<xsl:value-of select="getContent(gotoSubModel(),'category')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="sp:cat" mode="category">
		<xsl:variable name="fullName" select="setDialogVar('fullName','')"/>
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>