<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" xmlns:opa="kelis.fr:opa" exclude-result-prefixes="sc sp op opa">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:include href="intern:bswsp:~param/libMoodle/categoryName.xsl"/>

	<xsl:template match="opa:assmntNodelet">
		<question type="category">
			<category>
				<text>
					<xsl:value-of select="$fullName" disable-output-escaping="yes"/>
				</text>
			</category>
		</question>
		<xsl:apply-templates select="sp:quiz"/>
	</xsl:template>

	<xsl:template match="sp:quiz">
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="*"/>
</xsl:stylesheet>