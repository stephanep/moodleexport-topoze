<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  xmlns:java="http://xml.apache.org/xslt/java"
  exclude-result-prefixes="sc sp op java">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>
<xsl:variable name="vRandom" select="java:java.util.Random.new()"/>

  <xsl:template match="op:labelImg">
    <xsl:apply-templates select="sp:img"/>
  </xsl:template>

  <xsl:template match="sp:img">
    <xsl:call-template name="image" />
  </xsl:template>

  <xsl:template name="image">
    <xsl:variable name="quote">"</xsl:variable>
    <xsl:variable name="tag"><xsl:value-of select="getContent(gotoSubModel(),'ico')" disable-output-escaping="yes" /></xsl:variable>
    <xsl:variable name="src" select="substring-before(substring-after($tag,'res/'),$quote)" />
    <xsl:variable name="width" select="substring-before(substring-after($tag,concat('width=',$quote)),$quote)" />
    <xsl:variable name="height" select="substring-before(substring-after($tag,concat('height=',$quote)),$quote)" />
    <xsl:variable name="new">
      <xsl:copy-of select="getDialogVar('matchCo')"/>
      <label type="img" title="" src="{$src}" width="{$width}" height="{$height}" random="{java:nextDouble($vRandom)}"/>
    </xsl:variable>
    <xsl:variable name="matchCo" select="setDialogVar('matchCo',$new)"/>
  </xsl:template>

  <xsl:template match="*"/>

</xsl:stylesheet>