<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" exclude-result-prefixes="sc sp op">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:include href="intern:bswsp:~param/libMoodle/question.xsl"/>
	<xsl:variable name="QuizNameInTitle" select="getDialogVar('QuizNameInTitle')"/>
	<xsl:variable name="vPos" select="getDialogVar('vPos')"/>
	<xsl:variable name="forceQuizOrder" select="getDialogVar('forceQuizOrder')"/>
	<xsl:template match="op:numerical">
		<question type="numerical">
			<xsl:variable name="title" select="normalize-space(getContent(gotoMeta(),''))"/>
			<name>
				<text>
					<xsl:if test="$forceQuizOrder = 'yes'">
						<xsl:value-of select="format-number($vPos, '0000')"/>
						<xsl:text> </xsl:text>
					</xsl:if>
					<xsl:if test="$QuizNameInTitle = 'yes'">
						<xsl:value-of select="substring-before(extractFileNameFromPath(srcFields(srcFileAgent(), 'srcUri')), '.quiz')"/>
						<xsl:text>
						</xsl:text>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="string-length($title) &gt; 0">
							<xsl:value-of select="$title"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="sc:question" mode="title"/>
						</xsl:otherwise>
					</xsl:choose>
				</text>
			</name>
			<xsl:apply-templates select="sc:question"/>
			<xsl:apply-templates select="sc:globalExplanation"/>
			<defaultgrade>1.0000000</defaultgrade>
			<penalty>0.1000000</penalty>
			<hidden>0</hidden>
			<xsl:apply-templates select="sp:asw/sc:NFSolution"/>
			<xsl:value-of select="getContent(gotoMeta(),'tags')" disable-output-escaping="yes"/>
		</question>
	</xsl:template>
	<xsl:template match="sp:asw/sc:NFSolution">
		<answer fraction="100" format="moodle_auto_format">
			<text>
				<xsl:value-of select="sc:value"/>
			</text>
			<feedback format="html">
				<text/>
			</feedback>
			<xsl:choose>
				<xsl:when test="sc:condition[@operation='+-']">
					<tolerance>
						<xsl:value-of select="sc:condition"/>
					</tolerance>
				</xsl:when>
				<xsl:when test="sc:condition[@operation='%']">
					<tolerance>
						<xsl:value-of select="number(sc:value) * number(sc:condition) div 100"/>
					</tolerance>
				</xsl:when>
				<xsl:otherwise>
					<tolerance>0</tolerance>
				</xsl:otherwise>
			</xsl:choose>
		</answer>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>