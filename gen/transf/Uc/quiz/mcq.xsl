<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" exclude-result-prefixes="sc sp op">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:include href="intern:bswsp:~param/libMoodle/question.xsl"/>
	<xsl:variable name="vPos" select="getDialogVar('vPos')"/>
	<xsl:variable name="forceQuizOrder" select="getDialogVar('forceQuizOrder')"/>
	<xsl:variable name="shuffle" select="getDialogVar('shuffle')"/>
	<xsl:variable name="QuizNameInTitle" select="getDialogVar('QuizNameInTitle')"/>
	<xsl:variable name="numbering" select="getDialogVar('numbering')"/>
	<xsl:variable name="solPos" select="number(//op:mcqSur/sc:solution/@choice)"/>
	<!-- nombre de bonnes de réponses dans le QCM et valeurs en % de chacune -->
	<xsl:variable name="checked" select="count(//op:mcqMur/sc:choices/sc:choice[@solution='checked'])"/>
	<xsl:variable name="checkedFraction">
		<xsl:choose>
			<xsl:when test="$checked = 1">100</xsl:when>
			<xsl:when test="$checked = 2">50</xsl:when>
			<xsl:when test="$checked = 3">33.33333</xsl:when>
			<xsl:when test="$checked = 4">25</xsl:when>
			<xsl:when test="$checked = 5">20</xsl:when>
			<xsl:when test="$checked = 6">16.66667</xsl:when>
			<xsl:when test="$checked = 7">14.28571</xsl:when>
			<xsl:when test="$checked = 8">12.5</xsl:when>
			<xsl:when test="$checked = 9">11.11111</xsl:when>
			<xsl:when test="$checked = 10">10</xsl:when>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="exportExpGlob" select="getDialogVar('exportExpGlob')"/>
	<xsl:template match="op:mcqSur | op:mcqMur">
		<question type="multichoice">
			<xsl:variable name="title" select="normalize-space(getContent(gotoMeta(),''))"/>
			<name>
				<text>
					<xsl:if test="$forceQuizOrder = 'yes'">
						<xsl:value-of select="format-number($vPos, '0000')"/>
						<xsl:text>
						</xsl:text>
					</xsl:if>
					<xsl:if test="$QuizNameInTitle = 'yes'">
						<xsl:value-of select="substring-before(extractFileNameFromPath(srcFields(srcFileAgent(), 'srcUri')), '.quiz')"/>
						<xsl:text>
						</xsl:text>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="string-length($title) &gt; 0">
							<xsl:value-of select="$title"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="sc:question" mode="title"/>
						</xsl:otherwise>
					</xsl:choose>
				</text>
			</name>
			<xsl:apply-templates select="sc:question"/>
			<xsl:if test="$exportExpGlob!='yes'">
				<xsl:apply-templates select="sc:globalExplanation"/>
			</xsl:if>
			<defaultgrade>1.0000000</defaultgrade>
			<penalty>0.1000000</penalty>
			<hidden>0</hidden>
			<shuffleanswers>
				<!--
    -->
				<xsl:if test="$shuffle = 'no'">0</xsl:if>
				<!--
    -->
				<xsl:if test="$shuffle = 'yes'">1</xsl:if>
				<!--
  -->
			</shuffleanswers>
			<!-- type QCM ou QCU -->
			<xsl:choose>
				<xsl:when test="self::op:mcqSur">
					<single>true</single>
				</xsl:when>
				<xsl:when test="self::op:mcqMur">
					<single>false</single>
				</xsl:when>
			</xsl:choose>
			<answernumbering>
				<xsl:value-of select="$numbering"/>
			</answernumbering>
			<!-- position de la bonne réponse du QCU -->
			<xsl:variable name="solPos" select="number(sc:solution/@choice)"/>
			<!-- nombre de bonnes de réponses dans le QCM  et leur valeur en % -->
			<xsl:variable name="checked" select="count(sc:choices/sc:choice[@solution='checked'])"/>
			<xsl:variable name="unchecked" select="count(sc:choices/sc:choice[@solution='unchecked'])"/>
			<xsl:variable name="checkedFraction">
				<xsl:choose>
					<xsl:when test="$checked = 1">100</xsl:when>
					<xsl:when test="$checked = 2">50</xsl:when>
					<xsl:when test="$checked = 3">33.33333</xsl:when>
					<xsl:when test="$checked = 4">25</xsl:when>
					<xsl:when test="$checked = 5">20</xsl:when>
					<xsl:when test="$checked = 6">16.66667</xsl:when>
					<xsl:when test="$checked = 7">14.28571</xsl:when>
					<xsl:when test="$checked = 8">12.5</xsl:when>
					<xsl:when test="$checked = 9">11.11111</xsl:when>
					<xsl:when test="$checked = 10">10</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="uncheckedFraction">
				<xsl:choose>
					<xsl:when test="$unchecked = 0">0</xsl:when>
					<xsl:when test="$unchecked = 1">-100</xsl:when>
					<xsl:when test="$unchecked = 2">-50</xsl:when>
					<xsl:when test="$unchecked = 3">-33.33333</xsl:when>
					<xsl:when test="$unchecked = 4">-25</xsl:when>
					<xsl:when test="$unchecked = 5">-20</xsl:when>
					<xsl:when test="$unchecked = 6">-16.66667</xsl:when>
					<xsl:when test="$unchecked = 7">-14.28571</xsl:when>
					<xsl:when test="$unchecked = 8">-12.5</xsl:when>
					<xsl:when test="$unchecked = 9">-11.11111</xsl:when>
					<xsl:when test="$unchecked = 10">-10</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:apply-templates select="sc:choices/sc:choice">
				<xsl:with-param name="solPos" select="$solPos"/>
				<xsl:with-param name="checkedFraction" select="$checkedFraction"/>
				<xsl:with-param name="uncheckedFraction" select="$uncheckedFraction"/>
			</xsl:apply-templates>
			<xsl:value-of select="getContent(gotoMeta(),'tags')" disable-output-escaping="yes"/>
		</question>
	</xsl:template>
	<xsl:template match="sc:choice">
		<xsl:param name="checkedFraction"/>
		<xsl:param name="uncheckedFraction"/>
		<xsl:param name="solPos"/>
		<xsl:choose>
			<xsl:when test="@solution = 'checked' ">
				<answer fraction="{$checkedFraction}">
					<xsl:attribute name="format">html</xsl:attribute>
					<xsl:apply-templates select="sc:*"/>
					<xsl:call-template name="exportExpGlob"/>
				</answer>
			</xsl:when>
			<xsl:when test="@solution = 'unchecked' ">
				<answer fraction="{$uncheckedFraction}">
					<xsl:attribute name="format">html</xsl:attribute>
					<xsl:apply-templates select="sc:*"/>
					<xsl:call-template name="exportExpGlob"/>
				</answer>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="fraction">
					<xsl:if test="position() = $solPos">100</xsl:if>
					<xsl:if test="position() != $solPos">0</xsl:if>
				</xsl:variable>
				<answer fraction="{$fraction}">
					<xsl:attribute name="format">html</xsl:attribute>
					<xsl:apply-templates select="sc:*"/>
					<xsl:call-template name="exportExpGlob"/>
				</answer>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="exportExpGlob">
		<xsl:if test="$exportExpGlob='yes' and not(sc:choiceExplanation)">
			<xsl:call-template name="cdata">
				<xsl:with-param name="elt" select="'feedback'"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template match="sc:choiceLabel">
		<xsl:variable name="init_files" select="setDialogVar('files','')"/>
		<text>
			<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
		</text>
		<xsl:value-of select="getDialogVar('files')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="sc:choiceExplanation">
		<xsl:call-template name="cdata">
			<xsl:with-param name="elt" select="'feedback'"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>