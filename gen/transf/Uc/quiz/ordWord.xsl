<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="sc sp op java">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:include href="intern:bswsp:~param/libMoodle/question.xsl"/>
	<xsl:variable name="QuizNameInTitle" select="getDialogVar('QuizNameInTitle')"/>
	<xsl:variable name="vPos" select="getDialogVar('vPos')"/>
	<xsl:variable name="forceQuizOrder" select="getDialogVar('forceQuizOrder')"/>
	<xsl:variable name="vRandom" select="java:java.util.Random.new()"/>
	<xsl:variable name="type" select="getDialogVar('ordWordType')"/>
	<xsl:template match="op:ordWord">
		<xsl:choose>
			<xsl:when test="$type = 'clozeP' or $type = 'clozeUL' or $type = 'clozeOL'">
				<question type="cloze">
					<xsl:variable name="title" select="normalize-space(getContent(gotoMeta(),''))"/>
					<name>
						<text>
							<xsl:if test="$forceQuizOrder = 'yes'">
								<xsl:value-of select="format-number($vPos, '0000')"/>
								<xsl:text> </xsl:text>
							</xsl:if>
							<xsl:if test="$QuizNameInTitle = 'yes'">
								<xsl:value-of select="substring-before(extractFileNameFromPath(srcFields(srcFileAgent(), 'srcUri')), '.quiz')"/>
								<xsl:text>
								</xsl:text>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="string-length($title) &gt; 0">
									<xsl:value-of select="$title"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:apply-templates select="sc:question" mode="title"/>
								</xsl:otherwise>
							</xsl:choose>
						</text>
					</name>
					<questiontext>
						<text>
							<xsl:apply-templates select="sc:question" mode="cloze"/>
							<xsl:choose>
								<xsl:when test="$type = 'clozeP'">
									<p>
										<xsl:apply-templates select="sc:label"/>
									</p>
								</xsl:when>
								<xsl:when test="$type = 'clozeUL'">
									<ul>
										<xsl:apply-templates select="sc:label" mode="list"/>
									</ul>
								</xsl:when>
								<xsl:when test="$type = 'clozeOL'">
									<ol>
										<xsl:apply-templates select="sc:label" mode="list"/>
									</ol>
								</xsl:when>
							</xsl:choose>
						</text>
						<xsl:value-of select="getDialogVar('files')" disable-output-escaping="yes"/>
					</questiontext>
					<xsl:apply-templates select="sc:globalExplanation"/>
					<shuffleanswers>0</shuffleanswers>
					<xsl:value-of select="getContent(gotoMeta(),'tags')" disable-output-escaping="yes"/>
				</question>
			</xsl:when>
			<xsl:when test="$type = 'matching' ">
				<question type="matching">
					<xsl:variable name="title" select="normalize-space(getContent(gotoMeta(),''))"/>
					<name>
						<text>
							<xsl:choose>
								<xsl:when test="string-length($title) &gt; 0">
									<xsl:value-of select="$title"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:apply-templates select="sc:question" mode="title"/>
								</xsl:otherwise>
							</xsl:choose>
						</text>
					</name>
					<xsl:apply-templates select="sc:question"/>
					<xsl:apply-templates select="sc:globalExplanation"/>
					<shuffleanswers>0</shuffleanswers>
					<xsl:for-each select="sc:label">
						<xsl:variable name="label" select="getContent(gotoSubModel(),'')"/>
						<subquestion format="html">
							<text>
								<p>
									<xsl:value-of select="position()"/>
								</p>
							</text>
							<answer>
								<text>
									<xsl:value-of select="$label"/>
								</text>
							</answer>
						</subquestion>
					</xsl:for-each>
					<xsl:value-of select="getContent(gotoMeta(),'tags')" disable-output-escaping="yes"/>
				</question>
			</xsl:when>
			<xsl:otherwise>
				<p>Problème : ordWordType = "<xsl:value-of select="$type"/>"</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="sc:question" mode="cloze">
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="sc:label">
		<!--
-->
		<xsl:variable name="good">
			<xsl:value-of select="getContent(gotoSubModel(),'')"/>
		</xsl:variable><!--
-->{1:MULTICHOICE:<!--
--><xsl:for-each select="//sc:label">
			<xsl:sort select="java:nextDouble($vRandom)" data-type="number"/>
			<xsl:variable name="label" select="getContent(gotoSubModel(),'')"/>
			<xsl:if test="position() != 1">~</xsl:if>
			<xsl:if test="$good = $label">=</xsl:if>
			<xsl:value-of select="$label"/>
		</xsl:for-each>}  <!--
--></xsl:template>
	<xsl:template match="sc:label" mode="list">
		<!--
-->
		<xsl:variable name="good">
			<xsl:value-of select="getContent(gotoSubModel(),'')"/>
		</xsl:variable>
		<!--
-->
		<li>{1:MULTICHOICE:<!--
--><xsl:for-each select="//sc:label">
				<xsl:sort select="java:nextDouble($vRandom)" data-type="number"/>
				<xsl:variable name="label" select="getContent(gotoSubModel(),'')"/>
				<xsl:if test="position() != 1">~</xsl:if>
				<xsl:if test="$good = $label">=</xsl:if>
				<xsl:value-of select="$label"/>
			</xsl:for-each>}</li>
		<!--
-->
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>