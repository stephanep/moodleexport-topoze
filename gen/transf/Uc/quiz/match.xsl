<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" exclude-result-prefixes="sc sp op">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:include href="intern:bswsp:~param/libMoodle/question.xsl"/>
	<xsl:variable name="QuizNameInTitle" select="getDialogVar('QuizNameInTitle')"/>
	<xsl:variable name="vPos" select="getDialogVar('vPos')"/>
	<xsl:variable name="forceQuizOrder" select="getDialogVar('forceQuizOrder')"/>
	<xsl:variable name="shuffle" select="getDialogVar('shuffle')"/>
	<xsl:template match="op:match">
		<xsl:variable name="init">
			<init/>
		</xsl:variable>
		<xsl:variable name="initMatchCo" select="setDialogVar('matchCo',$init)"/>
		<question type="matching">
			<xsl:variable name="title" select="normalize-space(getContent(gotoMeta(),''))"/>
			<name>
				<text>
					<xsl:if test="$forceQuizOrder = 'yes'">
						<xsl:value-of select="format-number($vPos, '0000')"/>
						<xsl:text>
						</xsl:text>
					</xsl:if>
					<xsl:if test="$QuizNameInTitle = 'yes'">
						<xsl:value-of select="substring-before(extractFileNameFromPath(srcFields(srcFileAgent(), 'srcUri')), '.quiz')"/>
						<xsl:text>
						</xsl:text>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="string-length($title) &gt; 0">
							<xsl:value-of select="$title"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="sc:question" mode="title"/>
						</xsl:otherwise>
					</xsl:choose>
				</text>
			</name>
			<xsl:apply-templates select="sc:question"/>
			<xsl:apply-templates select="sc:globalExplanation"/>
			<defaultgrade>1.0000000</defaultgrade>
			<penalty>0.1000000</penalty>
			<hidden>0</hidden>
			<shuffleanswers>
				<!--
    -->
				<xsl:if test="$shuffle = 'false'">0</xsl:if>
				<!--
    -->
				<xsl:if test="$shuffle = 'true'">1</xsl:if>
				<!--
  -->
			</shuffleanswers>
			<xsl:call-template name="createSubquestions"/>
			<xsl:value-of select="getContent(gotoMeta(),'tags')" disable-output-escaping="yes"/>
		</question>
	</xsl:template>
	<xsl:template match="sc:group">
		<xsl:apply-templates select="sc:*"/>
	</xsl:template>
	<xsl:template match="sc:target">
		<xsl:variable name="axisTarget" select="setDialogVar('axis','target')"/>
		<xsl:variable name="target" select="getContent(gotoSubModel(),'')"/>
	</xsl:template>
	<xsl:template match="sc:label">
		<xsl:variable name="axisLabel" select="setDialogVar('axis','label')"/>
		<xsl:variable name="label" select="getContent(gotoSubModel(),'')"/>
	</xsl:template>
	<xsl:template name="createSubquestions">
		<xsl:apply-templates select="sc:group"/>
		<xsl:variable name="matchCo" select="getDialogVar('matchCo')"/>
		<xsl:for-each select="$matchCo/label">
			<xsl:sort select="number(@random)" data-type="number"/>
			<subquestion format="html">
				<text>
					<xsl:choose>
						<xsl:when test="@type = 'text'">
							<p>
								<xsl:value-of select="@title"/>
							</p>
						</xsl:when>
						<xsl:when test="@type = 'img'">
							<img src="{concat('@@PLUGINFILE@@/',@src)}" width="{@width}" height="{@height}"/>
						</xsl:when>
					</xsl:choose>
				</text>
				<xsl:if test="./@type = 'img'">
					<file name="{@src}" encoding="base64"/>
				</xsl:if>
				<answer format="html">
					<text>
						<xsl:value-of select="./preceding-sibling::target[1]/@title"/>
					</text>
				</answer>
			</subquestion>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>